package org.polytechtours.javaperformance.tp.paintingants;
// package PaintingAnts_v2;

import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;

// version : 2.0

import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * <p>
 * Titre : Painting Ants
 * </p>
 * <p>
 * Description :
 * </p>
 * <p>
 * Copyright : Copyright (c) 2003
 * </p>
 * <p>
 * Société : Equipe Réseaux/TIC - Laboratoire d'Informatique de l'Université de
 * Tours
 * </p>
 *
 * @author Nicolas Monmarché
 * @version 1.0
 */

public class CPainting extends Canvas implements MouseListener {
    private static final long serialVersionUID = 1L;
    // matrices servant pour le produit de convolution
    MatriceConv mMatriceConv9 = new MatriceConv(9);
    MatriceConv mMatriceConv25 = new MatriceConv(25);
    MatriceConv mMatriceConv49 = new MatriceConv(49);
    // Objet de type Graphics permettant de manipuler l'affichage du Canvas
    private Graphics mGraphics;
    // Objet ne servant que pour les bloc synchronized pour la manipulation du
    // tableau des couleurs
    private Object mMutexCouleurs = new Object();
    // tableau des couleurs, il permert de conserver en memoire l'état de chaque
    // pixel du canvas, ce qui est necessaire au deplacemet des fourmi
    // il sert aussi pour la fonction paint du Canvas
    private MatriceCouleur mCouleurs;
    // couleur du fond
    private static int fondR = 255;
    private static int fondG= 255;
    private static int fondB = 255;
    private Color mCouleurFond = new Color(fondR, fondG, fondB); // TODO changer les getRed/Green/Blue par fondR/G/B
    // dimensions
    private Dimension mDimension = new Dimension();

    private PaintingAnts mApplis;

    private boolean mSuspendu = false;

    /******************************************************************************
     * Titre : public CPainting() Description : Constructeur de la classe
     ******************************************************************************/
    public CPainting(Dimension pDimension, PaintingAnts pApplis) {
        int i, j;
        addMouseListener(this);

        mApplis = pApplis;

        mDimension = pDimension;
        setBounds(new Rectangle(0, 0, mDimension.width, mDimension.height));

        this.setBackground(mCouleurFond);

        // initialisation de la matrice des couleurs
        mCouleurs = new MatriceCouleur(mDimension.width, mDimension.height);
        synchronized (mMutexCouleurs) {
            for (i = 0; i != mDimension.width; i++) {
                for (j = 0; j != mDimension.height; j++) {
                    mCouleurs.setValeursRGB(i, j, fondR, fondG, fondB);
                }
            }
        }

    }

    /******************************************************************************
     * Titre : Color getCouleur Description : Cette fonction renvoie la couleur
     * d'une case
     ******************************************************************************/
    public Color getCouleur(int x, int y) {
        synchronized (mMutexCouleurs) {
            return mCouleurs.toColor(x, y);
        }
    }

    /******************************************************************************
     * Titre : Color getDimension Description : Cette fonction renvoie la
     * dimension de la peinture
     ******************************************************************************/
    public Dimension getDimension() {
        return mDimension;
    }

    /******************************************************************************
     * Titre : Color getHauteur Description : Cette fonction renvoie la hauteur de
     * la peinture
     ******************************************************************************/
    public int getHauteur() {
        return mDimension.height;
    }

    /******************************************************************************
     * Titre : Color getLargeur Description : Cette fonction renvoie la hauteur de
     * la peinture
     ******************************************************************************/
    public int getLargeur() {
        return mDimension.width;
    }

    /******************************************************************************
     * Titre : void init() Description : Initialise le fond a la couleur blanche
     * et initialise le tableau des couleurs avec la couleur blanche
     ******************************************************************************/
    public void init() {
        int i, j;
        mGraphics = getGraphics();
        synchronized (mMutexCouleurs) {
            mGraphics.clearRect(0, 0, mDimension.width, mDimension.height);

            // initialisation de la matrice des couleurs

            for (i = 0; i != mDimension.width; i++) {
                for (j = 0; j != mDimension.height; j++) {
                    mCouleurs.setValeursRGB(i, j, fondR, fondG, fondB);
                }
            }
        }

        // initialisation de la matrice de convolution : lissage moyen sur 9
        // cases
        /*
         * 1 2 1 2 4 2 1 2 1
         */
        mMatriceConv9.initMatrice();

        // initialisation de la matrice de convolution : lissage moyen sur 25
        // cases
        /*
         * 1 1 2 1 1 1 2 3 2 1 2 3 4 3 2 1 2 3 2 1 1 1 2 1 1
         */
        mMatriceConv25.initMatrice();

        // initialisation de la matrice de convolution : lissage moyen sur 49
        // cases
        /*
         * 1 1 2 2 2 1 1 1 2 3 4 3 2 1 2 3 4 5 4 3 2 2 4 5 8 5 4 2 2 3 4 5 4 3 2 1 2
         * 3 4 3 2 1 1 1 2 2 2 1 1
         */
        mMatriceConv49.initMatrice();

        mSuspendu = false;
    }

    /****************************************************************************/
    public void mouseClicked(MouseEvent pMouseEvent) {
        pMouseEvent.consume();
        if (pMouseEvent.getButton() == MouseEvent.BUTTON1) {
            // double clic sur le bouton gauche = effacer et recommencer
            if (pMouseEvent.getClickCount() == 2) {
                init();
            }
            // simple clic = suspendre les calculs et l'affichage
            mApplis.pause();
        } else {
            // bouton du milieu (roulette) = suspendre l'affichage mais
            // continuer les calculs
            if (pMouseEvent.getButton() == MouseEvent.BUTTON2) {
                suspendre();
            } else {
                // clic bouton droit = effacer et recommencer
                // case pMouseEvent.BUTTON3:
                init();
            }
        }
    }

    /****************************************************************************/
    public void mouseEntered(MouseEvent pMouseEvent) {
    }

    /****************************************************************************/
    public void mouseExited(MouseEvent pMouseEvent) {
    }

    /****************************************************************************/
    public void mousePressed(MouseEvent pMouseEvent) {

    }

    /****************************************************************************/
    public void mouseReleased(MouseEvent pMouseEvent) {
    }

    /******************************************************************************
     * Titre : void paint(Graphics g) Description : Surcharge de la fonction qui
     * est appelé lorsque le composant doit être redessiné
     ******************************************************************************/
    @Override
    public void paint(Graphics pGraphics) {
        int i, j;

        synchronized (mMutexCouleurs) {
            for (i = 0; i < mDimension.width; i++) {
                for (j = 0; j < mDimension.height; j++) {
                    pGraphics.setColor(mCouleurs.toColor(i, j));
                    pGraphics.fillRect(i, j, 1, 1);
                }
            }
        }
    }

    /******************************************************************************
     * Titre : void colorer_case(int x, int y, Color c) Description : Cette
     * fonction va colorer le pixel correspondant et mettre a jour le tabmleau des
     * couleurs
     ******************************************************************************/
    public void setCouleur(int x, int y, Color c, int pTaille) {
        synchronized (mMutexCouleurs) {
            if (!mSuspendu) {
                // on colorie la case sur laquelle se trouve la fourmi
                mGraphics.setColor(c);
                mGraphics.fillRect(x, y, 1, 1);
            }

            mCouleurs.setValeurFromColor(x, y, c);

            // on fait diffuser la couleur :
            switch (pTaille) {
                case 1:
                    switch1(x,y);
                    // produit de convolution discrete sur 9 cases

                    break;
                case 2:
                    switch2(x,y);
                    // produit de convolution discrete sur 25 cases
                    break;
                case 3:
                    switch3(x,y);
                    // produit de convolution discrete sur 49 cases
                    break;
            }// end switch
        }
    }

    /******************************************************************************
     * Titre : setSupendu Description : Cette fonction change l'état de suspension
     ******************************************************************************/
    public void suspendre() {
        mSuspendu = !mSuspendu;
        if (!mSuspendu) {
            repaint();
        }
    }

    public void switch1(int x, int y){
        int i, j, k, l, m, n;
        float R, G, B;
        Color lColor;

        for (i = 0; i < 3; i++) {
            for (j = 0; j < 3; j++) {
                R = G = B = 0f;

                for (k = 0; k < 3; k++) {
                    for (l = 0; l < 3; l++) {
                        m = (x + i + k - 2 + mDimension.width) % mDimension.width;
                        n = (y + j + l - 2 + mDimension.height) % mDimension.height;
                        R += mMatriceConv9.getValeur(k, l) * mCouleurs.getRed(m, n);
                        G += mMatriceConv9.getValeur(k, l) * mCouleurs.getGreen(m, n);
                        B += mMatriceConv9.getValeur(k, l) * mCouleurs.getBlue(m, n);
                    }
                }
                lColor = new Color((int) R, (int) G, (int) B);

                mGraphics.setColor(lColor);

                m = (x + i - 1 + mDimension.width) % mDimension.width;
                n = (y + j - 1 + mDimension.height) % mDimension.height;
                mCouleurs.setValeurFromColor(m, n, lColor);
                if (!mSuspendu) {
                    mGraphics.fillRect(m, n, 1, 1);
                }
            }
        }
    }

    public void switch2(int x, int y){
        int i, j, k, l, m, n;
        float R, G, B;
        Color lColor;

        for (i = 0; i < 5; i++) {
            for (j = 0; j < 5; j++) {
                R = G = B = 0f;

                for (k = 0; k < 5; k++) {
                    for (l = 0; l < 5; l++) {
                        m = (x + i + k - 4 + mDimension.width) % mDimension.width;
                        n = (y + j + l - 4 + mDimension.height) % mDimension.height;
                        R += mMatriceConv25.getValeur(k, l) * mCouleurs.getRed(m, n);
                        G += mMatriceConv25.getValeur(k, l) * mCouleurs.getGreen(m, n);
                        B += mMatriceConv25.getValeur(k, l) * mCouleurs.getBlue(m, n);
                    }
                }
                lColor = new Color((int) R, (int) G, (int) B);
                mGraphics.setColor(lColor);
                m = (x + i - 2 + mDimension.width) % mDimension.width;
                n = (y + j - 2 + mDimension.height) % mDimension.height;

                mCouleurs.setValeurFromColor(m, n, lColor);
                if (!mSuspendu) {
                    mGraphics.fillRect(m, n, 1, 1);
                }

            }
        }
    }

    public void switch3(int x, int y){
        int i, j;
        float [] RGB;

        // produit de convolution discrete sur 49 cases
        for (i = 0; i < 7; i++) {
            for (j = 0; j < 7; j++) {

                RGB = rbgTraitement(x,y,j,i);
                graphTraitement(RGB,x,y,i,j);
            }
        }
    }

    public float[] rbgTraitement(int x, int y, int j, int i){
        int k, l;
        float [] RGB = {0f,0f,0f,0f,0f};
        for (k = 0; k < 7; k++) {
            for (l = 0; l < 7; l++) {
                RGB[3] = (x + i + k - 6 + mDimension.width) % mDimension.width;
                RGB[4] = (y + j + l - 6 + mDimension.height) % mDimension.height;
                RGB[0] += mMatriceConv49.getValeur(k, l) * mCouleurs.getRed((int)RGB[3], (int)RGB[4]);
                RGB[1] += mMatriceConv49.getValeur(k, l) * mCouleurs.getGreen((int)RGB[3], (int)RGB[4]);
                RGB[2] += mMatriceConv49.getValeur(k, l) * mCouleurs.getBlue((int)RGB[3], (int)RGB[4]);
            }
        }
       return RGB;
    }

    public void graphTraitement(float [] RGB, int x, int y, int i, int j){
        Color lColor;

        lColor = new Color((int) RGB[0], (int) RGB[1], (int) RGB[2]);
        mGraphics.setColor(lColor);
        RGB[3] = (x + i - 3 + mDimension.width) % mDimension.width;
        RGB[4] = (y + j - 3 + mDimension.height) % mDimension.height;

        valeurFromColor(RGB,lColor);
    }

    public void valeurFromColor(float[]RGB,Color lColor){
        mCouleurs.setValeurFromColor((int)RGB[3], (int)RGB[4], lColor);
        graphFill(RGB);
    }
    public void graphFill(float[]RGB){
        if (!mSuspendu) {
            mGraphics.fillRect((int) RGB[3], (int)RGB[4], 1, 1);
        }
    }

}
