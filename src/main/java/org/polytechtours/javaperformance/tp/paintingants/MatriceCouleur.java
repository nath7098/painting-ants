package org.polytechtours.javaperformance.tp.paintingants;

import java.awt.*;

public class MatriceCouleur {
    // largeur de la matrice
    private int largeur;
    // hauteur de la matirce
    private int hauteur;
    // matrice de couleur
    private int[][][] matrice;

    public MatriceCouleur(int largeur, int hauteur) {
        this.largeur = largeur;
        this.hauteur = hauteur;
        matrice = new int[largeur][hauteur][3];
    }

    public int[][][] getMatrice() {
        return matrice;
    }

    public int[] getValeursRGB(int i, int j) {
        return matrice[i][j];
    }

    public void setValeursRGB(int i, int j, int r, int g, int b) {
        matrice[i][j][0] = r;
        matrice[i][j][1] = g;
        matrice[i][j][2] = b;
    }

    public int getRed(int i, int j) {
        return matrice[i][j][0];
    }

    public int getGreen(int i, int j) {
        return matrice[i][j][1];
    }
    public int getBlue(int i, int j) {
        return matrice[i][j][2];
    }

    public void setValeurFromColor(int i, int j, Color color) {
        setValeursRGB(i, j, color.getRed(), color.getGreen(), color.getBlue());
    }

    public Color toColor(int i, int j) {
        return new Color(getRed(i, j), getGreen(i, j), getBlue(i, j));
    }

}
