package org.polytechtours.javaperformance.tp.paintingants;

public class MatriceConv {

    // taillede la matrice
    private int tailleMatrice;
    // matrice pour le produit de convolution
    private float[][] matrice;

    /******************************************************************************
     * Titre : public MatriceConv Description : Constructeur de la classe
     ******************************************************************************/
    public MatriceConv(int tailleMatrice) {
        this.tailleMatrice = tailleMatrice;
        this.matrice = new float[(int)Math.sqrt(tailleMatrice)][(int)Math.sqrt(tailleMatrice)];
    }

    public int getTailleMatrice() {
        return tailleMatrice;
    }

    public float[][] getMatrice() {
        return matrice;
    }

    public float getValeur(int k, int l) {
        return matrice[k][l];
    }   

    public void initMatrice() {
        switch (tailleMatrice) {
            case 9:
                matrice[0][0] = 1 / 16f;    //  //  //  //  //
                matrice[0][1] = 2 / 16f;    //  1   2   1   //
                matrice[0][2] = 1 / 16f;    //  2   4   2   //
                matrice[1][0] = 2 / 16f;    //  1   2   1   //
                matrice[1][1] = 4 / 16f;    //  //  //  //  //
                matrice[1][2] = 2 / 16f;
                matrice[2][0] = 1 / 16f;
                matrice[2][1] = 2 / 16f;
                matrice[2][2] = 1 / 16f;
                break;
            case 25:
                matrice[0][0] = 1 / 44f;
                matrice[0][1] = 1 / 44f;
                matrice[0][2] = 2 / 44f;
                matrice[0][3] = 1 / 44f;
                matrice[0][4] = 1 / 44f;
                matrice[1][0] = 1 / 44f;
                matrice[1][1] = 2 / 44f;    //  //  //  //  //  //  //
                matrice[1][2] = 3 / 44f;    //  1   1   2   1   1   //
                matrice[1][3] = 2 / 44f;    //  1   2   3   2   1   //
                matrice[1][4] = 1 / 44f;    //  2   3   4   3   2   //
                matrice[2][0] = 2 / 44f;    //  1   2   3   2   1   //
                matrice[2][1] = 3 / 44f;    //  1   1   2   1   1   //
                matrice[2][2] = 4 / 44f;    //  //  //  //  //  //  //
                matrice[2][3] = 3 / 44f;
                matrice[2][4] = 2 / 44f;
                matrice[3][0] = 1 / 44f;
                matrice[3][1] = 2 / 44f;
                matrice[3][2] = 3 / 44f;
                matrice[3][3] = 2 / 44f;
                matrice[3][4] = 1 / 44f;
                matrice[4][0] = 1 / 44f;
                matrice[4][1] = 1 / 44f;
                matrice[4][2] = 2 / 44f;
                matrice[4][3] = 1 / 44f;
                matrice[4][4] = 1 / 44f;
                break;
            case 49:
                matrice[0][0] = 1 / 128f;
                matrice[0][1] = 1 / 128f;
                matrice[0][2] = 2 / 128f;
                matrice[0][3] = 2 / 128f;
                matrice[0][4] = 2 / 128f;
                matrice[0][5] = 1 / 128f;
                matrice[0][6] = 1 / 128f;

                matrice[1][0] = 1 / 128f;
                matrice[1][1] = 2 / 128f;
                matrice[1][2] = 3 / 128f;
                matrice[1][3] = 4 / 128f;
                matrice[1][4] = 3 / 128f;   //  //  //  //  //  //  //  //  //
                matrice[1][5] = 2 / 128f;   //  1   1   2   2   2   1   1   //
                matrice[1][6] = 1 / 128f;   //  1   2   3   4   3   2   1   //
                                            //  2   3   4   5   4   3   2   //
                matrice[2][0] = 2 / 128f;   //  2   4   5   8   5   4   2   //
                matrice[2][1] = 3 / 128f;   //  2   3   4   5   4   3   2   //
                matrice[2][2] = 4 / 128f;   //  1   2   3   4   3   2   1   //
                matrice[2][3] = 5 / 128f;   //  1   1   2   2   2   1   1   //
                matrice[2][4] = 4 / 128f;   //  //  //  //  //  //  //  //  //
                matrice[2][5] = 3 / 128f;
                matrice[2][6] = 2 / 128f;

                matrice[3][0] = 2 / 128f;
                matrice[3][1] = 4 / 128f;
                matrice[3][2] = 5 / 128f;
                matrice[3][3] = 8 / 128f;
                matrice[3][4] = 5 / 128f;
                matrice[3][5] = 4 / 128f;
                matrice[3][6] = 2 / 128f;

                matrice[4][0] = 2 / 128f;
                matrice[4][1] = 3 / 128f;
                matrice[4][2] = 4 / 128f;
                matrice[4][3] = 5 / 128f;
                matrice[4][4] = 4 / 128f;
                matrice[4][5] = 3 / 128f;
                matrice[4][6] = 2 / 128f;

                matrice[5][0] = 1 / 128f;
                matrice[5][1] = 2 / 128f;
                matrice[5][2] = 3 / 128f;
                matrice[5][3] = 4 / 128f;
                matrice[5][4] = 3 / 128f;
                matrice[5][5] = 2 / 128f;
                matrice[5][6] = 1 / 128f;

                matrice[6][0] = 1 / 128f;
                matrice[6][1] = 1 / 128f;
                matrice[6][2] = 2 / 128f;
                matrice[6][3] = 2 / 128f;
                matrice[6][4] = 2 / 128f;
                matrice[6][5] = 1 / 128f;
                matrice[6][6] = 1 / 128f;
                break;
            default:
                break;
        }
    }

}
