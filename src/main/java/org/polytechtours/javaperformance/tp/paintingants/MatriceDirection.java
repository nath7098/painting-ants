package org.polytechtours.javaperformance.tp.paintingants;

public class MatriceDirection {
    // taillede la matrice
    private int tailleMatrice1;
    private int tailleMatrice2;
    // matrice pour le produit de convolution
    private static int[][] matrice;

    public MatriceDirection(int taille1, int taille2) {
        this.tailleMatrice1 = taille1;
        this.tailleMatrice2 = taille2;
        this.matrice = new int[tailleMatrice1][tailleMatrice2];
    }

    public int getTailleMatrice1() {
        return tailleMatrice1;
    }
    public int getTailleMatrice2() {
        return tailleMatrice2;
    }

    public int[][] getMatrice() {
        return matrice;
    }

    public void initMatrice() {
        matrice[0][0] = 0;
        matrice[0][1] = -1;
        matrice[1][0] = 1;
        matrice[1][1] = -1;
        matrice[2][0] = 1;
        matrice[2][1] = 0;
        matrice[3][0] = 1;
        matrice[3][1] = 1;
        matrice[4][0] = 0;
        matrice[4][1] = 1;
        matrice[5][0] = -1;
        matrice[5][1] = 1;
        matrice[6][0] = -1;
        matrice[6][1] = 0;
        matrice[7][0] = -1;
        matrice[7][1] = -1;
    }
}
